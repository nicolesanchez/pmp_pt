<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Status;
use App\Models\OrderLine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class OrderController extends Controller
{
    public function index()
    {
        return view('order.index');
    }

    /**
     * Para visualizar el listado de pedidos
     */
    public function getOrders(){
        $orders = Order::withTrashed()->get(); // Recoge todos los pedidos, incluso los eliminados.

        $jsonData = [];
        foreach ($orders as $order){
            // Calculamos la suma de artículos de cada pedido.
            $totalQty = DB::table('order_lines')
                        ->where('order_id', $order->id)
                        ->whereNull('deleted_at')
                        ->sum('qty_client');

            $jsonData[] = [
                'id' => $order->id,
                'reference' => $order->reference,
                'status_id' => $order->status->name ? $order->status->name : null,
                'created_at' => $order->created_at ? $order->created_at->format('d-m-Y') : null,
                'updated_at' => $order->updated_at ? $order->updated_at->format('d-m-Y') : null,
                'deleted_at' => $order->deleted_at ? $order->deleted_at->format('d-m-Y') : null,
                'total_qty' => $totalQty
            ];
        }

        return response()->json(['data' => $jsonData]);
    }

    /**
     * Función para crear un nuevo pedido o modificarlo.
     * Si viene el $id, es modificación, si no, es creación.
     */
    public function editOrder($id = null){
        $order = $orderLines = null;

        if ($id) {
            $order = Order::findOrFail($id);  // Encuentra el pedido junto con sus orderLines o falla si no existe
        }

        // Así recogemos el listado de estados sin el estado ELIMINADO
        $statusList = Status::excludeId(5)->get();

        return view('forms.edit-order', compact('order', 'statusList'));
    }

    /**
     * Guarda los datos de un pedido
     */
    public function saveOrder(Request $request){

        // Validar datos del formulario
        $validatedData = $request->validate([
            'reference' => 'required|unique:orders,reference|max:100',
            'status' => 'required',
            'orderLines.*.item_ref' => 'required',
            'orderLines.*.qty_client' => 'required|numeric|min:1'
        ]);

        // Crear un nuevo pedido
        $order = new Order();
        $order->reference = $validatedData['reference'];
        $order->status_id = $validatedData['status'];
        $order->save(); // Hace el guardado en la base de datos.

        // Guardamos cada línea de pedido
        foreach ($request->orderLines as $ol) {
            $orderLine = new OrderLine();
            $orderLine->order_id = $order->id; // Guardamos el ID del pedido que acabamos de crear
            $orderLine->item_ref = $ol['item_ref'];
            $orderLine->qty_client = $ol['qty_client'];
            $orderLine->qty_wh = $ol['qty_client']; // Guardamos la misma cantidad, ya que es una simulación.
            $orderLine->save(); // Guardar la línea de pedido en la base de datos
        }

        // Redirige a la tabla principal
        return redirect()->route('order.index');
    }

    /**
     * Modifica los datos de un pedido
     */
    public function updateOrder(Request $request, $id){
        // Validar datos del formulario excluyendo el $id actual para que podamos actualizar el status "repitiendo" referencia.
        $validatedData = $request->validate([
            'reference' => 'required|max:100|unique:orders,reference,' . $id,
            'status' => 'required',
            'orderLines.*.item_ref' => 'required',
            'orderLines.*.qty_client' => 'required|numeric|min:1'
        ]);

        $order = Order::findOrFail($id);
        $order->reference = $validatedData['reference'];
        $order->status_id = $validatedData['status'];
        $order->save();

        // Actualizar o crear nuevas líneas de pedido
        foreach ($request->orderLines as $lineData) {
            if(isset($lineData['id']) && !empty($lineData['id'])) {
                // Actualización de una línea existente
                $orderLine = OrderLine::find($lineData['id']);
                if ($orderLine) {
                    $orderLine->item_ref = $lineData['item_ref'];
                    $orderLine->qty_client = $lineData['qty_client'];
                    $orderLine->qty_wh = $lineData['qty_client'];
                    $orderLine->save();
                }
            } else {
                // Creación de una nueva línea de pedido
                $newOrderLine = new OrderLine();
                $newOrderLine->order_id = $order->id;
                $newOrderLine->item_ref = $lineData['item_ref'];
                $newOrderLine->qty_client = $lineData['qty_client'];
                $newOrderLine->qty_wh = $lineData['qty_client'];
                $newOrderLine->save();
            }
        }

        // Redirige a la tabla principal
        return redirect()->route('order.index');
    }

    /**
     * Para eliminar un pedido
     */
    public function deleteOrder($id){
        $order = Order::find($id);

        if (!$order) {
            return response()->json(['message' => 'No se ha encontrado el pedido.'], 404);
        }

        $order->status_id = 5; // Status nuevo: ELIMINADO
        $order->save(); // Se guardan los cambios

        $order->delete(); // Realiza el soft-delete y pone la fecha de eliminación.
        return response()->json(['message' => 'Pedido eliminado con éxito.']);
    }

    
}
