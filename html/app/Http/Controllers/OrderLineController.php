<?php

namespace App\Http\Controllers;

use App\Models\OrderLine;
use Illuminate\Http\Request;


class OrderLineController extends Controller
{
    /**
     * Para eliminar una línea de  pedido
     */
    public function deleteOrderLine($id){
        $orderLine = OrderLine::find($id);

        if (!$orderLine) {
            return response()->json([
                'success' => false,
                'message' => 'No se ha encontrado la línea.'
            ], 404);
        }

        $orderLine->delete(); // Realiza el soft-delete y pone la fecha de eliminación.
        return response()->json([
            'success' => true,
            'message' => 'Línea de pedido eliminada con éxito.'
        ]);
    }
}
