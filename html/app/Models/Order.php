<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{

    use SoftDeletes;

    protected $table = 'orders';
    protected $fillable = ['reference'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function orderLines()
{
    return $this->hasMany(OrderLine::class);
}
}
