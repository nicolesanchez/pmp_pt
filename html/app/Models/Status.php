<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Status extends Model
{

    protected $table = 'status';
    protected $fillable = ['name'];

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function scopeExcludeId($query, $id){
        return $query->where('id', '!=', $id);
    }

}
