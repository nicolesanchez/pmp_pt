<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class OrderLine extends Model
{

    use SoftDeletes;

    protected $table = 'order_lines';
    protected $fillable = ['qty_client', 'qty_wh', 'item_ref', 'order_id'];
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
