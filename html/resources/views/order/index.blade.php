<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
@extends('layout.app')

@section('content')
    <div class="container-fluid">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="row">
            <div class="col-lg-12">
                <div class="card w-100">
                    <div class="card-body">
                        <div class="mb-3">
                            <h5 class="card-title fw-semibold">Pedidos</h5>
                        </div>
                        <!-- Filtros a desarrollar si se quiere -->
                        <div>
                            <button id="create-new-order" class="general-button">Crear pedido</button>
                        </div>
                        <!-- Tabla -->
                        <div class="table-responsive">
                            <table id="orderTable" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr class="table-rows">
                                    <th class="table-columns">Id Pedido</th>
                                    <th class="table-columns">Referencia</th>
                                    <th class="table-columns">Estado</th>
                                    <th class="table-columns">F. Creación</th>
                                    <th class="table-columns">F. Actualización</th>
                                    <th class="table-columns">F. Borrado</th>
                                    <th class="table-columns">Artículos</th>
                                    <th class="table-columns"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de Confirmación -->
    <div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Confirmar Eliminación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            ¿Estás seguro de que deseas eliminar este pedido?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger" id="confirmDelete">Eliminar</button>
        </div>
        </div>
    </div>
    </div>

    <script>

        $(document).ready(function () {

            let ajaxConfig = {
                url: "{{route('order.getOrders')}}",
                type: "GET",
                contentType: "application/json",
            };

            $('#orderTable').DataTable({
                dom: '<"row align-items-center"<"col-9"f><"col-3"l>>t<"row align-items-center"<"col-6"i><"col-6">>',
                lengthMenu: [10, 25, 50, 100, 200],
                processing: true,
                serverSide: false,
                searching: false,
                ajax: ajaxConfig,
                columns: [
                    {
                        data: 'id',
                        name: "id",
                        orderable: true,
                    },
                    {
                        data: 'reference',
                        name: "reference",
                        orderable: false,
                        searchable: true,
                    },
                    {
                        data: 'status_id',
                        name: "status_id",
                        orderable: true,
                        searchable: false,
                    },
                    {
                        data: 'created_at',
                        name: "created_at",
                        orderable: true,
                        searchable: false,
                        render: function (data, type, row ){
                            return data ? data : '-';
                        }
                    },
                    {
                        data: 'updated_at',
                        name: "updated_at",
                        orderable: true,
                        searchable: false,
                        render: function (data, type, row ){
                            return data ? data : '-';
                        }
                    },
                    {
                        data: 'deleted_at',
                        name: "deleted_at",
                        orderable: true,
                        searchable: false,
                        render: function (data, type, row ){
                            return data ? data : '-';
                        }
                    },
                    {
                        data: 'total_qty',
                        name: 'total_qty',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            return data ? data : '0';
                        }
                    },
                    {
                        data: null,
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            if (row.status_id === 'Eliminado') {
                                return ''; // No mostrar botones si el status_id es ELIMINADO
                            } else {
                                return `<a href="/orders/${data.id}/edit_order" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                                        <button id="del-order" type="button" class="btn btn-danger btn-sm" data-id="${data.id}" data-toggle="modal" data-target="#deleteConfirmationModal"><i class="fas fa-trash-alt"></i></button>`;
                            }
                        }
                    },
                ],
                order: [1, 'desc'],
                paging: false,
                rowReorder: false,
                orderCellsTop: true,
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.5/i18n/es-ES.json',
                    info: "",
                    infoEmpty: "",
                },
                drawCallback: function () {
                    toggleLoading(false)

                }
            });

            //Función que muestra un spiner de carga de datos
            function toggleLoading(state, text=null, icon=null) {
                if (state) {
                        Swal.fire({
                            title: text,
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            html: '<i class="fas fa-spinner fa-spin"></i>',
                            // onBeforeOpen: () => {
                            //     Swal.showLoading();
                            // }
                        });
                } else {
                    Swal.close();
                }
            }
        });

        // Evento para el botón de crear pedido
        $('#create-new-order').on('click', function() {
            window.location.href = "{{route('order.newOrder')}}"; // Redirecciona al usuario a la ruta del formulario
        });

        // Para recoger el ID del pedido a eliminar
        $('#deleteConfirmationModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Esto recoge el botón que ha disparado el evento.
            var id = button.data('id');
            var modal = $(this);
            modal.find('.btn-danger').data('id', id); // Pasa el ID al botón de confirmar
        });

        // Botón dentro del modal para confirmar la eliminación
        $('#confirmDelete').on('click', function() {
            var id = $(this).data('id');
            // Llamada AJAX para eliminar el elemento
            $.ajax({
                url: `/orders/${id}`,
                type: 'POST',
                data: { _token: '{{ csrf_token() }}' },
                success: function(result) {
                    // Cierra el modal y recarga la página para que se visualice el cambio en la tabla
                    $('#deleteConfirmationModal').modal('hide');
                    location.reload();
                },
                error: function(err) {
                    alert('Error al eliminar el pedido.');
                }
            });

            $('#deleteConfirmationModal').modal('hide'); // Cierra el modal
        });

    </script>
@endsection
