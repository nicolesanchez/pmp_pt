<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
@extends('layout.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card w-100">
                <div class="card-body">
                    <div class="mb-3">
                        <h5 class="card-title fw-semibold">{{ $order ? 'Editar Pedido' : 'Nuevo Pedido' }}</h5>
                    </div>
                    <!-- Formulario -->
                    <form action="{{ $order ? route('order.updateOrder', $order->id) : route('order.saveOrder') }}" method="POST">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                        @csrf
                        @if($order)
                            @method('PUT')
                        @endif
                        <div class="form-group">
                            <label for="reference">N. Referencia:</label>
                            <input type="text" class="form-control" id="reference" name="reference" required placeholder="Introduce el número de referencia" value="{{ $order->reference ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="status">Estado:</label>
                            <select class="form-control" id="status" name="status" required>
                                @foreach($statusList as $status)
                                    <option value="{{ $status->id }}" {{ isset($order) && $order->status_id == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="mb-3 d-flex align-items-center">
                                <h4 class="mb-0 me-2">Líneas</h4>
                                <button type="button" class="btn btn-success rounded-circle" style="width: 24px; height: 24px; padding: 0; display: inline-flex; justify-content: center; align-items: center; font-size: 12px;" onclick="addNewLine()">
                                    <i class="fa fa-plus" style="color: white;"></i>
                                </button>
                            </div>
                            <div class="card mb-2" id="orderLines">
                                <div class="card-body">
                                    @if($order && $order->orderLines && $order->orderLines->count() > 0)
                                        @foreach ($order->orderLines as $index => $orderLine)
                                            <div class="row g-3" id="line{{ $index }}">
                                                <div class="col-md-4">
                                                    <label for="item_ref{{ $index }}" class="form-label">Artículo:</label>
                                                    <input type="text" class="form-control" id="item_ref{{ $index }}" name="orderLines[{{ $index }}][item_ref]" value="{{ $orderLine->item_ref }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="qty_client{{ $index }}" class="form-label">Cantidad:</label>
                                                    <input type="number" class="form-control" id="qty_client{{ $index }}" name="orderLines[{{ $index }}][qty_client]" value="{{ $orderLine->qty_client }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="button" class="btn btn-danger rounded-circle" style="width: 24px; height: 24px; padding: 0; display: inline-flex; justify-content: center; align-items: center; font-size: 12px;" onclick="deleteLine({{ $index }}, '{{ $orderLine->id ?? '' }}')">
                                                        <i class="fa fa-minus" style="color: white;"></i>
                                                    </button>
                                                </div>
                                                <input type="hidden" name="orderLines[{{ $index }}][id]" value="{{ $orderLine->id }}">
                                            </div>
                                        @endforeach
                                    @else
                                        <p id="noLinesMsg">No hay Líneas para este pedido.</p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">{{ $order ? 'Actualizar' : 'Crear' }} Pedido</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // Botón de añadir nueva línea
    let orderLineIndex = {{ $order->orderLines->count() ?? 0 }};
    function addNewLine() {
        // Selección del último 'card-body' dentro de los 'orderLines'
        const cardBody = document.querySelector('#orderLines .card-body');

        // Comprueba si existe el mensaje de "No hay líneas" y se elimina
        const noLinesMsg = document.getElementById('noLinesMsg');
        if (noLinesMsg && orderLineIndex === 0) {
            noLinesMsg.remove();
        }

        // Crear un nuevo 'div' para los inputs
        const newRow = document.createElement('div');
        newRow.className = 'row g-3';
        newRow.id = 'line' + orderLineIndex;
        newRow.innerHTML = `
            <div class="col-md-4">
                <label for="item_ref${orderLineIndex}" class="form-label">Artículo</label>
                <input type="text" class="form-control" id="item_ref${orderLineIndex}" name="orderLines[${orderLineIndex}][item_ref]">
            </div>
            <div class="col-md-4">
                <label for="qty_client${orderLineIndex}" class="form-label">Cantidad</label>
                <input type="number" class="form-control" id="qty_client${orderLineIndex}" name="orderLines[${orderLineIndex}][qty_client]">
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-danger rounded-circle delete-btn" style="width: 24px; height: 24px; padding: 0; display: inline-flex; justify-content: center; align-items: center; font-size: 12px;" onclick="deleteLine(${orderLineIndex}, null)">
                    <i class="fa fa-minus" style="color: white;"></i>
                </button>
            </div>`;

        // Añadir la nueva fila al 'card-body'
        cardBody.appendChild(newRow);

        // Incrementar el índice para futuros usos
        orderLineIndex++;
    }


    function deleteLine(index, id) {
        let lineDiv = document.getElementById('line' + index);
        if (id) {
            // Si tiene ID, hacemos una llamada AJAX para eliminarlo de la base de datos
            $.ajax({
                url: '/order_lines/' + id,
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function(response) {
                    alert(response.message);
                    if (response.success) {
                        location.reload(); // Recarga la página para reflejar los cambios
                    }
                }
            });
        } else {
            // Si no tiene ID, simplemente elimina la línea del DOM
            lineDiv.remove();
        }
    }


</script>

@endsection