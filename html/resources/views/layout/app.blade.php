<!doctype html>
<html lang="en">

@include('layout.head')

<body>
<div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
     data-sidebar-position="fixed" data-header-position="fixed">
    @include('layout.menuleft')
    <div class="body-wrapper">
        @yield('content')
    </div>
</div>
<script src="{{asset('storage/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('storage/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('storage/js/sidebarmenu.js')}}"></script>
<script src="{{asset('storage/js/app.min.js')}}"></script>
<script src="{{asset('storage/js/app.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</body>

</html>
