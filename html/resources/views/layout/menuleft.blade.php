<aside id="left-menu" class="left-sidebar">
    <div>
        <div class="brand-logo d-flex align-items-center justify-content-between">
            <a href="{{route('order.index')}}" class="text-nowrap logo-img">
                <img src="{{asset('storage/images/logo-pmp.svg')}}" width="180" alt="" />
            </a>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
            <ul id="sidebarnav">
                <li class="nav-small-cap">
                    <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
                    <span class="hide-menu">Home</span>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{route('order.index')}}" aria-expanded="false">
                <span>
                  <i class="ti ti-shopping-cart"></i>
                </span>
                        <span class="hide-menu">Pedidos</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
