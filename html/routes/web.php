<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderLineController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/* ORDERS */
Route::get('/', [OrderController::class, 'index'])->name('order.index');

Route::get('/orders', [OrderController::class, 'getOrders'])->name('order.getOrders');

/**
 * Las rutas de creación y modificación comparten la misma vista y el mismo método en el controlador.
 */
Route::get('/orders/new_order', [OrderController::class, 'editOrder'])->name('order.newOrder');
Route::get('/orders/{id}/edit_order', [OrderController::class, 'editOrder'])->name('order.editOrder');

Route::post('/orders/save_order', [OrderController::class, 'saveOrder'])->name('order.saveOrder');

Route::put('/orders/{id}', [OrderController::class, 'updateOrder'])->name('order.updateOrder');
Route::post('/orders/{id}', [OrderController::class, 'deleteOrder'])->name('order.deleteOrder');

/* ORDER LINES */
Route::post('/order_lines/{id}', [OrderLineController::class, 'deleteOrderLine'])->name('order.deleteOrderLine');
