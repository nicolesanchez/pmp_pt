# PRUEBA TÉCNICA PAMPLING

### 1. Levantar proyecto en Docker

- Abrir proyecto en el IDE deseado (recomendamos PHPStorm, por defecto VSC).
- Una vez abierto, levantamos desde una terminal el proyecto

```jsx
1. docker-compose build //Esperar a finalizar y lanzamos el segundo comando.
2. docker-compose up -d //Proyecto levantado y listo para funcionar MySQL - Apache (Laravel)
```

<aside>
⚠️ Si no se consigue levantar el proyecto en docker (*falta de instalación de WSL en el sistema o error en la configuración del proyecto)* se valorará positivamente el levantamiento en otro entorno Ej. Xamp, Wamp, Laragon. 
Finalmente aun sin conseguirlo se valorará el continuar realizando la prueba.

</aside>

### 2. Instalación Base de datos

- Abrir gestor de BBDD que más nos guste
- Realizar una nueva conexión MySQL usando los valores definidos en el proyecto
- Importación del archivo de la base de datos que se encuentra en el `.zip`

### 3. Configurar entorno Laravel

Con todo lo anterior ya debemos tener acceso a nuestro proyecto accediendo desde un navegador.

Para continuar es probable que tengas que solventar los siguientes errores:

> Error 1:
>
>
> ![img-error-1](/img-error-1.png)
>
> <aside>
> ⚠️ Para solventar este problema al acceder a la ruta base de nuestro proyecto tendrás que dar permisos a Apache con el usuario correspondiente en la carpeta correspondiente y configurar su nivel de acceso.
>
> </aside>
>

> Error 2:
>
>
> ![img-error-2](/img-error-2.png)
> 
> <aside>
> ⚠️ Para solventar este problema tendrás que generar una API KEY dentro del proyecto de laravel.
>
> </aside>
>
- Configurar variables de entorno para la conexión con base de datos.
- Creación del enlace simbólico para poder acceder al contenido de la carpeta storage desde public `/public→storage`

---

# Inicio de la Prueba

El proyecto consiste en una aplicación para el almacén logístico de una empresa. Dicha aplicación contiene la siguiente estructura.

**Pedidos: (orders)**

Los pedidos reflejan las compras realizadas por clientes y deben ser ingresados manualmente en el sistema por un operario (*CRUD Pedidos/Orders*). Estos pedidos pueden contener **uno o varios artículos** y se encontrarán reflejados en la tabla *order_lines*.

**Nunca** se podrá crear un pedido que no contenga artículos, y estos pedidos **deben reflejar la cantidad total de artículos solicitados por el cliente**.

**Líneas: (order_lines)**

Son los artículos que contiene un pedido.

Una línea está compuesta por las siguientes propiedades obligatorias:

- cantidad deseada (*solicitada por el cliente*).
- Cantidad enviada (*la que sale del almacén según el stock, aunque para este ejercicio esto es solo un dato ficticio, ya que no hay control de stock*).
- Código del Artículo. (Ficticio para la prueba, por ejemplo, Art-001, Art-002…)

**Estados: (status)**

Aquí se reflejan los estados por los que tiene que pasar un pedido. Estos cambios, para el ejercicio propuesto, se manejarán desde el detalle del pedido mediante un botón. Los estados de estos pedidos ya están definidos dentro de la base de datos.

**Olas: (waves)**

Las olas se crean para poder trabajar y procesar pedidos de manera más eficaz. De esta forma, una ola contiene **uno o varios pedidos**. Las olas se generarán desde una nueva pestaña y solamente se podrán asignar a ellas pedidos que se encuentren en estado **PENDIENTE**.

Estas olas también tienen su propio estado (para este ejercicio se pueden usar los mismos que están definidos en la tabla *status*).

El flujo de trabajo es el siguiente:

1. Se recibe un nuevo pedido. Para este caso, se dará de alta directamente desde la pestaña de pedidos.
2. Una vez registrados los pedidos, se sincronizan los que tienen estado pendiente para generar una ola.
3. Se van trabajando los diferentes pedidos dentro de una ola y cambiando sus estados hasta completarlos todos.
4. Cuando todos los pedidos de una ola se han completado, esta pasa a estar finalizada.

<aside>
📢 Es importante destacar que no todas las propiedades y tablas descritas se encuentran definidas dentro de la estructura de la base de datos. El candidato deberá incluir aquellas tablas, columnas y relaciones que no estén definidas.
Existe total libertad para crear o modificar elementos que mejoren la aplicación.

</aside>

## **1. Desarrollo del CRUD de Pedidos y sus Líneas**

Una vez que el proyecto Laravel se ha levantado correctamente y la base de datos ha sido instalada, el siguiente paso en la prueba técnica consiste en desarrollar un CRUD para los pedidos existentes en la aplicación. La vista correspondiente a los pedidos ya está implementada en el proyecto, sin embargo, la funcionalidad para obtener y mostrar los datos de los pedidos aún no está completa. Por lo tanto, el objetivo es desarrollar el backend necesario para que esta vista funcione correctamente.

### **Requisitos:**

1. Lógica a desarrollar: Se debe desarrollar lo necesarios para manejar las operaciones CRUD en relación con los pedidos y las líneas que lo componen. Esta lógica será responsable de interactuar con la base de datos y proporcionar los datos necesarios a la vista de pedidos.
2. **Total de cantidad por pedido:** Implementar la **feature** que permita visualizar la cantidad de artículos totales que tiene el pedido.

---

## **2. Desarrollo de la Vista de Olas de Pedidos**

Una vez completado el CRUD para los pedidos, el siguiente paso es desarrollar una nueva vista que muestre las olas de pedidos disponibles. Las olas de pedidos son agrupaciones de pedidos que pueden asignarse al almacén para su procesamiento eficiente.

### **Requisitos:**

1. **Vista de Olas de Pedidos**: Se debe desarrollar una nueva vista en la aplicación que muestre las olas de pedidos disponibles. Esta vista deberá mostrar información relevante sobre cada ola de pedido, como su estado actual y el número total de pedidos que contiene. Además se deberá poder gestionar las olas (CRUD).